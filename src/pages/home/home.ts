import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { File } from '@ionic-native/file';
import { DocumentViewer, DocumentViewerOptions } from '@ionic-native/document-viewer';
import { FileTransfer } from '@ionic-native/file-transfer';
import { HttpClient, HttpHeaders } from  '@angular/common/http';
import { FileOpener } from '@ionic-native/file-opener';
 
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  pdfFilePath: string = 'assets/doc/pdf-test.pdf'
 
  constructor(public navCtrl: NavController, 
    private document: DocumentViewer, 
    private file: File, 
    private transfer: FileTransfer, 
    private platform: Platform,
    private opener: FileOpener,
    private http: HttpClient ) { }
 
  openLocalPdf() {
    const options: DocumentViewerOptions = {
      title: 'My PDF'
    }
    this.document.viewDocument('./assets/doc/pdf-test.pdf', 'application/pdf', options);
  }
 
  downloadAndOpenPdf() {
    let path = null;
 
    if (this.platform.is('ios')) {
      path = this.file.documentsDirectory;
    } else if (this.platform.is('android')) {
      path = this.file.dataDirectory;
    }
 
    const transfer = this.transfer.create();
    transfer.download('http://www.orimi.com/pdf-test.pdf', path + 'pdf-test.pdf').then(entry => {
      let url = entry.toURL();
      this.document.viewDocument(url, 'application/pdf', {});
    });
  }

  downloadOpener() {
    let headers = new HttpHeaders({'Content-Type': 'application/pdf'});
    this.http.get('http://unec.edu.az/application/uploads/2014/12/pdf-sample.pdf', { headers, observe: 'response', responseType: 'blob' } )
    .toPromise()
    .then( (response: any) => {
      console.log(response.body);
      const fileBlob: Blob = new Blob([response.body], {type: 'application/pdf'});
      this.file.writeFile(this.file.externalApplicationStorageDirectory, 'teste.pdf', fileBlob, { replace: true }).then( (res) => {
        // let filePath = URL.createObjectURL(fileBlob);
        console.log('Resposta write', res);
        console.log('URL.createObjectURL(fileBlob)', URL.createObjectURL(fileBlob));
        console.log('res.toInternalURL()', res.toInternalURL() );
        

        this.opener.open(res.toInternalURL(), 'application/pdf').then( () => {
          console.log('file opened successfully'); 
        })
        .catch( e =>  console.log('Error status: ' + e.status + ' - Error message: ' + e.message) );
      });

    });
  }

  downloadFileShowPDF() {
    let headers = new HttpHeaders({'Content-Type': 'application/pdf'});
    this.http.get('http://unec.edu.az/application/uploads/2014/12/pdf-sample.pdf', { headers, observe: 'response', responseType: 'blob' } )
    .toPromise()
    .then( (response: any) => {
      console.log(response.body);
      const file: Blob = new Blob([response.body], {type: 'application/pdf'});
      this.pdfFilePath = URL.createObjectURL(file);
    });
  }
}